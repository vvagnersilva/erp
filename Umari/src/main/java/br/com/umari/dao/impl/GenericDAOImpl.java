package br.com.umari.dao.impl;

import java.lang.reflect.ParameterizedType;
import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.com.umari.dao.GenericDAO;

@SuppressWarnings("unchecked")
public class GenericDAOImpl<T, PK> implements GenericDAO<T, PK> {

	@PersistenceContext(unitName = "UmariPU")
	protected EntityManager entityManager;

	
	
	public void persist(T entity) {
		System.out.println("Entrou em genericDAO");
		entityManager.persist(entity);
	}

	public void marge(T entity) {
		entityManager.merge(entity);

	}

	public void remove(T entity) {
		entityManager.remove(entity);
	}

	public void removeById(PK id) {
		T entity = getByID(id);
		entityManager.remove(entity);
	}

	public T getByID(PK id) {
		return (T) entityManager.find(getTypeClass(), id);
	}

	public Collection<T> findAll() {
		System.out.println("Listar todos");
		return entityManager.createQuery("FROM" + getTypeClass().getName()).getResultList();
	}

	public Query createQuery(String query, Object... parameters) {
		Query q = entityManager.createQuery(query);

		for (int i = 1; i < parameters.length; i++) {
			q.setParameter(i, parameters[i]);
		}
		return q;
	}

	private Class<?> getTypeClass() {
		Class<?> clazz = (Class<?>) ((ParameterizedType) this.getClass().getGenericSuperclass())
				.getActualTypeArguments()[1];
		return clazz;
	}

}
