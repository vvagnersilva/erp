package br.com.umari.dao;

import br.com.umari.entities.Estabelecimento;

public interface EstabelecimentoDAO extends GenericDAO<Estabelecimento, Long>{

}
