package br.com.umari.dao;

import java.util.Collection;

import javax.persistence.Query;

public interface GenericDAO <T,PK>{
	void persist(T entity);
	
	void marge(T entity);
	
	void remove(T entity);
	
	void removeById(PK id);
	
	T getByID(PK id);
	
	Collection<T> findAll();
	
	Query createQuery(String query,Object ... parameters);
}
